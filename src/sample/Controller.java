package sample;

import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Controller {

    @FXML
    private TextField txtCustomerID;

    @FXML
    private TextField txtCustomerName;

    @FXML
    private TextField txtCustomerState;

    @FXML
    private CheckBox chkRetailCustomer;

    @FXML
    private TextField txtCost;

    @FXML
    private TextField txtSalesTax;

    @FXML
    private TextField txtShipping;

    @FXML
    private TextField txtTotal;

    @FXML
    private RadioButton radUPS;

    @FXML
    private ToggleGroup ShippingGroup;

    @FXML
    private RadioButton radUSPostal;

    @FXML
    private RadioButton radFedGround;

    @FXML
    private RadioButton radFedAir;

    @FXML
    private TextField txtPartNumber;

    @FXML
    private TextField txtDescription;

    @FXML
    private TextField txtPricePerPart;

    @FXML
    private TextField txtQuantity;

    @FXML
    private CheckBox chkOversize;

    @FXML
    private Button btnCompute;

    @FXML
    private Button btnNewOrder;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnFile;

    @FXML
    private Button btnActions;

    @FXML
    private Button btnHelp;

    @FXML
    void btnActionsClick(MouseEvent event) {

    }

    @FXML
    void btnComputeClick(MouseEvent event) {
        if (txtCustomerName.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter customer name !");
            alert.setHeaderText("Error!");
            alert.show();
            return;
        }
        if (txtCustomerState.getText().length() != 2) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter a valid state !");
            alert.setHeaderText("Error!");
            alert.show();
            return;
        }
        if (txtPartNumber.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter part number !");
            alert.setHeaderText("Error!");
            alert.show();
            return;
        }
        if (txtDescription.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter part description !");
            alert.setHeaderText("Error!");
            alert.show();
            return;
        }
        try {
            if (Double.parseDouble(txtPricePerPart.getText()) <= 0) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please enter a valid price !");
                alert.setHeaderText("Error!");
                alert.show();
                return;
            }
            if (Integer.parseInt(txtQuantity.getText()) <= 0) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please enter a valid price !");
                alert.setHeaderText("Error!");
                alert.show();
                return;
            }
            double pricePerPart = Double.parseDouble(txtPricePerPart.getText());
            int quantity = Integer.parseInt(txtQuantity.getText());
            double salesTaxRate;
            double cost = pricePerPart * quantity;
            txtCost.setText("$" + Math.ceil(cost * 100) / 100);
            salesTaxRate = 0;
            if (chkRetailCustomer.isSelected()) {
                switch (txtCustomerState.getText()) {
                    case "CA":
                        salesTaxRate = 0.1;
                        break;
                    case "NY":
                    case "FL":
                        salesTaxRate = 0.05;
                        break;
                    default:
                        salesTaxRate = 0;
                        break;
                }
            }
            double salesTax = cost * salesTaxRate;
            txtSalesTax.setText("" + (Math.ceil(salesTax * 100) / 100));
            double shipping;
            switch (((RadioButton) ShippingGroup.getSelectedToggle()).getText()) {
                case "UPS":
                    shipping = 7;
                    break;
                case "Fed Ex Ground":
                    shipping = 9.25;
                    break;
                case "US Postal Air":
                    shipping = 8.5;
                    break;
                case "Fed Ex Air":
                    shipping = 12;
                    break;
                default:
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Please select a shipping method !");
                    alert.setHeaderText("Error!");
                    alert.show();
                    return;
            }
            if (chkOversize.isSelected())
                shipping += 5;
            shipping *= quantity;
            txtShipping.setText("" + (Math.ceil(shipping * 100) / 100));
            txtTotal.setText("$" + (Math.ceil((cost + salesTax + shipping)*100) / 100));
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please enter valid information!");
            alert.setHeaderText("Error!");
            alert.show();
            e.printStackTrace();
            return;
        }
    }

    @FXML
    void btnExitClick(MouseEvent event) {
        Stage stage = (Stage)btnExit.getScene().getWindow();
        stage.close();
    }

    @FXML
    void btnFileClick(MouseEvent event) {

    }

    @FXML
    void btnHelpClick(MouseEvent event) {

    }

    @FXML
    void btnNewOrderClick(MouseEvent event) {
        txtCustomerID.setText("");
        txtCustomerName.setText("");
        txtCustomerState.setText("");
        txtPartNumber.setText("");
        txtDescription.setText("");
        txtPricePerPart.setText("");
        txtQuantity.setText("");
        txtCost.setText("");
        txtSalesTax.setText("");
        txtShipping.setText("");
        txtTotal.setText("");
        chkOversize.setSelected(false);
        chkRetailCustomer.setSelected(true);
        radUPS.setSelected(true);
    }

    @FXML
    void txtPricePerPartTyped(KeyEvent event) {
        if (event.getCode().isLetterKey())
            txtPricePerPart.setText(txtPricePerPart.getText().substring(0, txtPricePerPart.getText().length() - 1));
    }

    @FXML
    void txtQuantityTyped(KeyEvent event) {
        if (event.getCode().isLetterKey())
            txtQuantity.setText(txtQuantity.getText().substring(0, txtQuantity.getText().length() - 1));
    }

    @FXML
    void txtStateTyped(KeyEvent event) {
        if (Character.isDigit(event.getCharacter().charAt(0)))
            event.consume();
    }

    @FXML
    void txtStateReleased(KeyEvent event) {
        if (txtCustomerState.getText().length() > 2)
            txtCustomerState.setText(txtCustomerState.getText().substring(0,2));
        txtCustomerState.setText(txtCustomerState.getText().toUpperCase());
        txtCustomerState.positionCaret(txtCustomerState.getText().length());
    }

    @FXML
    void txtStatePressed(KeyEvent event) {
//        txtCustomerState.setText(txtCustomerState.getText().toUpperCase());
//        txtCustomerState.positionCaret(txtCustomerState.getText().length());
    }

    @FXML
    public void initialize() {
        chkRetailCustomer.setSelected(true);
        chkOversize.setSelected(false);
        txtCost.setEditable(false);
        txtSalesTax.setEditable(false);
        txtShipping.setEditable(false);
        txtTotal.setEditable(false);
    }

}
